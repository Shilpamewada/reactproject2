let express = require('express');
let bodyparser = require('body-parser');
let cors =require("cors")

let app = express();
app.use(cors())
app.use(bodyparser.urlencoded({extended:false}))
app.use(bodyparser.json())

app.get("/",(req,res)=>{
    res.send("Home Page")
})

app.post("/postdata",(req,res)=>{
    res.send(req.body)
})


app.use('/login',require('./Routes/Login'))  

app.use('/register',require('./Routes/Register')) 



app.listen(3004,(req,res)=>{
    console.log("Server running")
})
